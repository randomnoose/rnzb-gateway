#!/usr/bin/env bash

# Remember to deploy metric-service on minikube cluster beforehand!

kubectl create -f ./deployments/rnzb-api-workers-deployment.yml
kubectl create -f ./deployments/rnzb-gateway-deployment.yml

kubectl create -f ./services/rnzb-api-worker-service.yml
kubectl create -f ./services/rnzb-gateway-service.yml

kubectl create -f ./horizontalPodAutoscaler/rnzb-api-worker-autoscaler.yaml